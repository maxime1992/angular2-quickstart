import {Hero} from './hero';

describe('Hero', () => {
	it('has name given in the constructor', () => {
		let hero = new Hero(1, 'Super Cat');
		expect(hero.name).toEqual('Super Cat');
	});
})