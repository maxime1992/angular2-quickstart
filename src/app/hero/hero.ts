 import {Component, bootstrap} from 'angular2/angular2';

let nextId = 30;
export class Hero {
	constructor(public id?: number, public name?: string, public power?: string, public alterEgo?: string) {
		this.id = id || nextId++;
	}
  
	clone() {
		return Hero.clone(this);
	}
	
  static clone = (h:any) => new Hero(h.id, h.name, h.alterEgo, h.power);
  static setNextId = (next:number) => nextId = next;
}

export class HeroesService {
	heroes: Array<Hero>;
	
	constructor() {
		this.heroes = [
			new Hero(11, "Mr. Nice"),
			// { "id": 12, "name": "Narco" },
			// { "id": 13, "name": "Bombasto" },
			// { "id": 14, "name": "Celeritas" },
			// { "id": 15, "name": "Magneta" },
			// { "id": 16, "name": "RubberMan" },
			// { "id": 17, "name": "Dynama" },
			// { "id": 18, "name": "Dr IQ" },
			// { "id": 19, "name": "Magma" },
			// { "id": 20, "name": "Tornado" }
		];
	}
}
