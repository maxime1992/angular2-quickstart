import {Component, bootstrap, Pipe, FORM_DIRECTIVES, CORE_DIRECTIVES} from 'angular2/angular2';
import {HeroesService, Hero} from './hero/hero'

@Pipe({
	name: 'modifyName'
})
class modifyNamePipe {
	transform(value: string, args: string[]) : any {
		return `${value}Test!!`;
		// return 'Test!!';
	}
}

@Component({
    selector: 'my-app',
	directives: [FORM_DIRECTIVES, CORE_DIRECTIVES],
	bindings: [HeroesService],
	pipes: [modifyNamePipe],
    template: `
		<h1>{{title}}</h1>
		<h2>My Heroes</h2>
		<ul class="heroes">
			<li *ng-for="#hero of heroes"
			    (click)="onSelect(hero)"
				[ng-class]="getSelectedClass(hero)">
				<span class="badge">{{hero.id}}</span> {{hero.name | modifyName}}
			</li>
		</ul>
		
		<div *ng-if="selectedHero">
			<h2>{{selectedHero.name}} details!</h2>
			<div><label>id: </label>{{selectedHero.id}}</div>
			<div>
				<label>name: </label>
				<input [(ng-model)]="selectedHero.name" placeholder="name"></input>
			</div>
		</div>
		
		<br>
		<br>
		<br>
		<div *ng-if="(delayedMessage | async)!=null">test de promise : {{ delayedMessage | async }}</div>
		
	`,
	styles:[`
	.heroes {list-style-type: none; margin-left: 1em; padding: 0; width: 10em;}
	.heroes li { cursor: pointer; position: relative; left: 0; transition: all 0.2s ease; }
	.heroes li:hover {color: #369; background-color: #EEE; left: .2em;}
	.heroes .badge {
		font-size: small;
		color: white;
		padding: 0.1em 0.7em;
		background-color: #369;
		line-height: 1em;
		position: relative;
		left: -1px;
		top: -1px;
	}
	.selected { background-color: #EEE; color: #369; }
	`]
})
class AppComponent {
	public title = 'Tour of Heroes';
	public selectedHero: Hero;
	public heroes;
	
	public delayedMessage: Promise<string> = new Promise((resolve, reject) => {
		setTimeout(() => resolve('You are a champion !'), 1500);
	});
	
	constructor (heroesService: HeroesService) {
		this.heroes = heroesService.heroes;
	}
	
	public onSelect(hero: Hero) {
		this.selectedHero = hero;
	}
	
	public getSelectedClass(hero: Hero) {
		return { 'selected': hero === this.selectedHero };
	}
}

bootstrap(AppComponent, [HeroesService]);